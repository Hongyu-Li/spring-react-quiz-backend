public class GetOrdersResponseForTest {
    private String name;
    private Double price;
    private Long quantity;
    private String unit;

    public GetOrdersResponseForTest() {
    }

    public GetOrdersResponseForTest(String name, Double price, Long quantity, String unit) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public String getUnit() {
        return unit;
    }
}
