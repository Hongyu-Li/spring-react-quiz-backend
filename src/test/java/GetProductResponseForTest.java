class GetProductResponseForTest {
    private Long id;
    private String name;
    private Double price;
    private String unit;
    private String url;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
