import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.domain.Orders;
import com.twuc.webApp.domain.OrdersRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import com.twuc.webApp.web.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ControllerTest extends ApiTestBase {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrdersRepository ordersRepository;

    private List<String> createProducts(List<Product> products) {
        List<String> names = new ArrayList<>();

        clear(em -> {
            productRepository.saveAll(products);
            productRepository.flush();
            names.addAll(products.stream().map(Product::getName).collect(Collectors.toList()));
        });
        return names;
    }

    private List<Long> createOrders(List<Orders> orders) {
        List<Long> ids = new ArrayList<>();

        clear(em -> {
            final List<Product> products = orders.stream().map(Orders::getProduct).collect(Collectors.toList());
            productRepository.saveAll(products);
            productRepository.flush();
            ordersRepository.saveAll(orders);
            ordersRepository.flush();
            ids.addAll(orders.stream().map(Orders::getId).collect(Collectors.toList()));
        });
        return ids;
    }


    @Test
    void should_get_all_products() throws Exception {
        final Product coke = new Product("可乐", 1.00, "瓶",
                "https://www.broudys.com/images/products/img6988.jpeg");
        final Product sprite = new Product("雪碧", 1.00, "瓶",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZ0m_CzJKJ2F7VIlx60MG1zyugrdNIKUl-ATkA0jbM4_lBB2Tb");
        createProducts(Arrays.asList(coke, sprite));

        String json = getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        GetProductResponseForTest[] products = new ObjectMapper().readValue(json, GetProductResponseForTest[].class);

        assertEquals(2, products.length);
        assertEquals(coke.getName(), products[0].getName());
        assertEquals(coke.getPrice(), products[0].getPrice());
        assertEquals(coke.getUnit(), products[0].getUnit());
        assertEquals(coke.getUrl(), products[0].getUrl());
        assertEquals(sprite.getName(), products[1].getName());
        assertEquals(sprite.getPrice(), products[1].getPrice());
        assertEquals(sprite.getUnit(), products[1].getUnit());
        assertEquals(sprite.getUrl(), products[1].getUrl());
    }

    @Test
    void should_get_zero_product() throws Exception {
        final String response = getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();
        GetProductResponseForTest[] content = new ObjectMapper().readValue(response, GetProductResponseForTest[].class);
        assertEquals(0,content.length);
    }

    @Test
    void should_create_product() throws Exception {
        final CreatePostRequestForTest request = new CreatePostRequestForTest("乐事", 5.00, "包",
                "http://a.ecimg.tw/items/DBAC0219008NK2D/000001_1514454166.jpg");
        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        request)))
                .andExpect(status().is(201));

            List<Product> products = productRepository.findAll();

            assertEquals(1, products.size());
            assertEquals(request.getName(), products.get(0).getName());
            assertEquals(request.getPrice(), products.get(0).getPrice());
            assertEquals(request.getUnit(), products.get(0).getUnit());
            assertEquals(request.getUrl(), products.get(0).getUrl());
    }

    @Test
    void should_not_create_product_twice() throws Exception {
        final CreatePostRequestForTest request1 = new CreatePostRequestForTest("乐事", 5.00, "包",
                "http://a.ecimg.tw/items/DBAC0219008NK2D/000001_1514454166.jpg");
        final CreatePostRequestForTest request2 = new CreatePostRequestForTest("乐事", 5.00, "包",
                "http://a.ecimg.tw/items/DBAC0219008NK2D/000001_1514454166.jpg");
        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        request1)))
                .andExpect(status().is(201));

        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        request2)))
                .andExpect(status().is(400))
                .andExpect(content().string("商品名称已存在，请输入新的商品名称"));
    }

    @Test
    void should_create_orders_table() throws Exception {
        final CreatePostRequestForTest product = new CreatePostRequestForTest("乐事", 5.00, "包",
                "http://a.ecimg.tw/items/DBAC0219008NK2D/000001_1514454166.jpg");
        productRepository.saveAndFlush(new Product(product.getName(), product.getPrice(), product.getUnit(), product.getUrl()));
        final CreateOrderRequestForTest request = new CreateOrderRequestForTest(product.getName());
        getMockMvc().perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        request)))
                .andExpect(status().is(201));
    }

    @Test
    void should_get_all_orders() throws Exception {
        final Orders cokeOrder1 = new Orders(new Product("可乐", 1.00, "瓶",
                "https://www.broudys.com/images/products/img6988.jpeg"));
        final Orders cokeOrder2 = new Orders(new Product("可乐", 1.00, "瓶",
                "https://www.broudys.com/images/products/img6988.jpeg"));
        final Orders spriteOrder = new Orders(new Product("雪碧", 1.00, "瓶",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZ0m_CzJKJ2F7VIlx60MG1zyugrdNIKUl-ATkA0jbM4_lBB2Tb"));
        createOrders(Arrays.asList(cokeOrder1,cokeOrder2,spriteOrder));

        String json = getMockMvc().perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        GetOrdersResponseForTest[] orders = new ObjectMapper().readValue(json, GetOrdersResponseForTest[].class);

        assertEquals(2, orders.length);
        assertEquals(cokeOrder1.getProduct().getName(), orders[0].getName());
        assertEquals(cokeOrder1.getProduct().getPrice(), orders[0].getPrice());
        assertEquals(cokeOrder1.getProduct().getUnit(), orders[0].getUnit());
        assertEquals(2, orders[0].getQuantity().longValue());
        assertEquals(spriteOrder.getProduct().getName(), orders[1].getName());
        assertEquals(spriteOrder.getProduct().getPrice(), orders[1].getPrice());
        assertEquals(spriteOrder.getProduct().getUnit(), orders[1].getUnit());
        assertEquals(1, orders[1].getQuantity().longValue());
    }

    @Test
    void should_delete_order() throws Exception {
        final CreatePostRequestForTest product = new CreatePostRequestForTest("乐事", 5.00, "包",
                "http://a.ecimg.tw/items/DBAC0219008NK2D/000001_1514454166.jpg");
        productRepository.saveAndFlush(new Product(product.getName(), product.getPrice(), product.getUnit(), product.getUrl()));
        final CreateOrderRequestForTest request = new CreateOrderRequestForTest(product.getName());
        getMockMvc().perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        request))).andReturn().getResponse();

        getMockMvc().perform(delete("/api/orders/乐事"))
                .andExpect(status().is(200));

        assertFalse(ordersRepository.findByProductName("乐事").isPresent());
    }
}


