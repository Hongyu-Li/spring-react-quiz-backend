class CreateOrderRequestForTest {
    private String name;

    public CreateOrderRequestForTest() {
    }

    public CreateOrderRequestForTest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
