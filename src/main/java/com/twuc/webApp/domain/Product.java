package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity(name= "products")
public class Product {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    @Id
    private String name;

    @Column
    private Double price;

    @Column
    private String unit;

    @Column
    private String url;

    public Product() {
    }

    public Product(String name, Double price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

//    public Long getId() {
//        return id;
//    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
