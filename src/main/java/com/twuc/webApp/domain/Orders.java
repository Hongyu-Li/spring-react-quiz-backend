package com.twuc.webApp.domain;

import com.twuc.webApp.contract.CreateProductRequest;

import javax.persistence.*;

@Entity(name= "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "productName")
    private Product product;

    public Orders() {
    }

    public Orders(Product product) {
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }
}
