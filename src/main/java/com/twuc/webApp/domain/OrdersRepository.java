package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrdersRepository extends JpaRepository<Orders,Long> {

    Long countByProductName(String name);

    @Query(value = "SELECT DISTINCT o.productName FROM orders o",nativeQuery = true)
    List<String> findDistinctName();

    void deleteByProductName(String name);

    Optional<Orders> findByProductName(String name);
}
