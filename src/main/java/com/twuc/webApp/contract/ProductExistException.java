package com.twuc.webApp.contract;


public class ProductExistException extends RuntimeException {
    public ProductExistException(String message) {
        super(message);
    }
}
