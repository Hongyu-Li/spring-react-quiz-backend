package com.twuc.webApp.contract;


public class CreateOrderRequest {

    private String name;

    public CreateOrderRequest() {
    }

    public CreateOrderRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
