package com.twuc.webApp.contract;

public class CreateProductRequest {
    private String name;
    private Double price;
    private String unit;
    private String url;

    public CreateProductRequest() {
    }

    public CreateProductRequest(String name, Double price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
