package com.twuc.webApp.web;

import com.twuc.webApp.contract.CannotDeleteException;
import com.twuc.webApp.contract.CreateOrderRequest;
import com.twuc.webApp.contract.GetOrdersResponse;
import com.twuc.webApp.domain.Orders;
import com.twuc.webApp.domain.OrdersRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {

    private OrdersRepository ordersRepository;
    private ProductRepository productRepository;

    public OrderController(OrdersRepository ordersRepository, ProductRepository productRepository) {
        this.ordersRepository = ordersRepository;
        this.productRepository = productRepository;
    }

    @PostMapping
    public ResponseEntity createOrders(@RequestBody CreateOrderRequest request){
        if (request == null) return ResponseEntity.badRequest().build();
        final Product product = productRepository.findByName(request.getName()).get();
        ordersRepository.save(new Orders(product));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity<List<GetOrdersResponse>> getAll(){
        final List<String> names = ordersRepository.findDistinctName();
        final List<Long> quantities = names.stream().map(item -> ordersRepository.countByProductName(item)).collect(Collectors.toList());
        final List<Product> productsInformation = names.stream().map(item -> productRepository.findByName(item).get()).collect(Collectors.toList());
        final List<GetOrdersResponse> orders = productsInformation.stream().map(item->
            new GetOrdersResponse(item.getName(),item.getPrice(),quantities.get(productsInformation.indexOf(item)),item.getUnit())).collect(Collectors.toList());
        return ResponseEntity.ok(orders);
    }

    @DeleteMapping("/{name}")
    @Transactional   //authorized deletion
    public ResponseEntity deleteOrders(@PathVariable String name){
        ordersRepository.deleteByProductName(name);
        if(ordersRepository.findByProductName(name).isPresent()){
            return ResponseEntity.ok().build();
        }else{
            throw new CannotDeleteException("订单删除失败,请稍后再试");
        }
    }
}
