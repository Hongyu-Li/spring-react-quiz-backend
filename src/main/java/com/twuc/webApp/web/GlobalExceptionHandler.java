package com.twuc.webApp.web;

import com.twuc.webApp.contract.CannotDeleteException;
import com.twuc.webApp.contract.ProductExistException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({ProductExistException.class, CannotDeleteException.class})
    public ResponseEntity handleException(Exception error) {
        return ResponseEntity.badRequest().body(error.getMessage());
    }
}
