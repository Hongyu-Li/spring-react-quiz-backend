package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.contract.ProductExistException;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {

    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<List<GetProductResponse>> getAll(){
        final List<GetProductResponse> products = productRepository.findAll(
                Sort.by(Sort.Direction.ASC, "name")).stream()
                .map(item -> new GetProductResponse(item.getName(), item.getPrice(), item.getUnit(), item.getUrl()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(products);
    }

    @PostMapping
    public ResponseEntity createProduct(@RequestBody CreateProductRequest request){
        if (request == null) return ResponseEntity.badRequest().build();
        if (productRepository.findByName(request.getName()).isPresent()){
            throw new ProductExistException("商品名称已存在，请输入新的商品名称");
        }
        productRepository.save(new Product(request.getName(), request.getPrice(),request.getUnit(),request.getUrl()));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}

