CREATE TABLE if not exists orders (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    productName varchar(64),
    FOREIGN KEY (productName) REFERENCES products(name)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;