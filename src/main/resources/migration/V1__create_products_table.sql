CREATE TABLE IF NOT EXISTS products (
#     id BIGINT PRIMARY KEY auto_increment,
    name varchar(64) PRIMARY KEY NOT NULL,
    price decimal(4,2) not null,
    unit varchar(8) not null,
#     url varchar(255) not null
    url text
)ENGINE=InnoDB DEFAULT CHARSET=utf8;