## Story 1 shopping mall homepage
1. configuration: gradle, properties & flyway migration.
2. productController: get all products api. 
3. homepage frontend which fetch data from backend api.


## Story 3 add products page
1. productController: post new product to repository
2. update homepage which fetch data from backend api
